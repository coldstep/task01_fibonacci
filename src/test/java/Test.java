/*
 * Copyright (c) Fibonacci Systems, Inc.
 * This software is first part of home task
 */

import com.fibonaccitask.*;
import java.util.Scanner;

/**
 * This class was created for tests
 *
 * @author Volodia Bakuro
 * @version 1.0
 * @since 2018-10-11
 */
public class Test {

  /**
   * main method
   *
   * @param args - standard argument for main method
   */
  public static void main(String[] args) {
    //just start a program

    startProgram();
  }

  /**
   * private method for test task one
   */
  private static void testTaskOne() {

    try(TaskOne taskOne = new TaskOne()){

      taskOne.getOddNumbers();
      taskOne.getEvenNumbers();

    }catch (Exception e){
      System.out.println(e.getMessage());
    }

  }

  /**
   * private method for test task two
   */
  private static void testTaskTwo() {
    new TaskTwo().getTaskTwo();
  }

  /**
   * private method which contain parts of interface
   */
  private static void startProgram() {
    System.out.println(
        "Please choose task which you want to begin \n"
            + "1. Task1 - (odds and over numbers)\n"
            + "2. Task2 - (fibonacci arrays)\n"
            + "3. Exit\n\n"
            + "Please enter number :"
    );
    switch (new Scanner(System.in).nextInt()) {
      case 1:
        testTaskOne();
        startProgram();
        break;
      case 2:
        testTaskTwo();
        startProgram();
        break;
      case 3:
        System.exit(0);
        break;
      default:
        System.out.println("It's not correct try write again or Exit\n"
            + "1. Try Again\n"
            + "2. Exit\n"
            + "Choose:");

        switch (new Scanner(System.in).nextInt()) {
          case 1:
            startProgram();
            break;
          case 2:
            break;
          default:
            break;
        }

        break;
    }


  }
}
