/*
 * Copyright (c) Fibonacci Systems, Inc.
 * This software is first part of home task
 */
/**
 * Package for homework.
 */
package com.fibonaccitask;

import com.sun.deploy.config.AutoUpdater;
import java.util.Scanner;

/**
 * This class was created for implement first part of homework.
 *
 * @author Volodia Bakuro
 * @version 1.0
 * @since 2018-10-11
 */
public class TaskOne implements AutoCloseable {

  /**
   * Variable for containing first number of interval.
   */
  private int start;
  /**
   * Variable for containing second number of interval.
   */
  private int end;

  /**
   * Method prints odd numbers from start to the end of interval.
   */
  public final void getOddNumbers() throws Exception {

    //Scanner gets numbers from console
    Scanner scanner = new Scanner(System.in);
    //Sum of odd numbers
    int sumOfOdds = 0;

    System.out.println(
        "Please enter interval for odd numbers: \n"
            + "From - ");
    //set first number of interval
    start = scanner.nextInt();

    System.out.println("To - ");
    // set second number of interval
    end = scanner.nextInt();

    if (end<=start){
      throw new Exception("\nYou can't set 'To' lower than 'from' !!!\n");
    }
    System.out.println("Your odd numbers are: ");
    //Loop give our sum and print array of odd numbers
    for (int i = start; i <= end; i++) {
      if (i % 2 != 0) {
        System.out.printf(i + " ");
        sumOfOdds += i;
      }
    }

    System.out.println("\nSum of odd numbers are - " + sumOfOdds);
  }

  /**
   * Method prints even numbers from end to the start of interval.
   */
  public final void getEvenNumbers() {

    //Scanner gets numbers from console
    Scanner scanner = new Scanner(System.in);
    //Sum of even numbers
    int sumOfEven = 0;

    System.out.println(
        "Please enter interval for even numbers : \n"
            + "From - ");
    //set first number of interval
    start = scanner.nextInt();

    System.out.println("To - ");
    // set second number of interval
    end = scanner.nextInt();

    System.out.println("Your even numbers are: ");
    //Loop give our sum and print array of even numbers
    for (int i = end; i >= start; i--) {
      if (i % 2 == 0) {
        System.out.printf(i + " ");
        sumOfEven += i;
      }
    }
    System.out.println("\nSum of odd numbers are - " + sumOfEven);
  }

  @Override
  public void close() throws Exception {
    System.out.println("\nTest of AutoCloseable interface\n");
    //only for fun
    start = 0;
    end = 0;
  }
}
