/*
 * Copyright (c) Fibonacci Systems, Inc.
 * This software is second part of home task
 */
/**
 * Package for homework.
 */
package com.fibonaccitask;

import java.util.Scanner;

/**
 * This class was created for implement second part of homework.
 *
 * @author Volodia Bakuro
 * @version 1.0
 * @since 2018-10-11
 */

public class TaskTwo {

  /**
   * Method prints interface for second task.
   */
  public final void getTaskTwo() {
    System.out.println("Please write number of set fibonacci array:\n"
        + "N = ");

    getFibonacciNumbers(new Scanner(System.in).nextInt());


  }

  /**
   * Method prints set of fibonacci numbers.
   *
   * @param n - amount of fibonacci numbers which must printed
   */
  private void getFibonacciNumbers(final int n) {

    try {

       if (n > 9) {
        throw new Exception(""
            + "\nToo many !!!!\n"
            + "Please set number lower than 90.\n"
        );
      } else if (n <= 0) {
        throw new Exception("\n Array can't be negative !!!! \n");
      }

      System.out.println("Your array:");

      final double percent = 100.0;
      // temporary variable for containing current number
      // of fibonacci array
      long temp;
      // contain previous number of fibonacci array
      long previousNumber = 0;
      // contain current number of fibonacci array
      long currentNumber = 1;
      // count of odd numbers
      long odds = 0;
      // count of even numbers
      long evens = 0;

      for (int i = 0; i < n; i++) {
        System.out.printf(currentNumber + " ");

        temp = currentNumber;
        currentNumber += previousNumber;
        previousNumber = temp;
        //check if current number is odd or even
        if (currentNumber % 2 == 0) {
          evens++;
        } else {
          odds++;
        }

      }

      System.out.println(
          "\n\nPercent of odd numbers is - "
              + (odds / (n / percent)) + "%"
      );
      System.out.println(
          "Percent of even numbers is - "
              + (evens / (n / percent)) + "%"
      );

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }finally {

    }
  }


}


